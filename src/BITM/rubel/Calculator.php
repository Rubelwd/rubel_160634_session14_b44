<?php


namespace App;


class Calculator
{
    private $number1;
    private $number2;
    private $operation;
    private $result;

    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function getNumber1()
    {
        return $this->number1;
    }

    public function getNumber2()
    {
        return $this->number2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function setResult()
    {
        switch($this->operation){
            case "Addition":
                $this->doAddition();
                break;

            case "Subtraction":
                $this->doSubtraction();
                break;

            case "Multiplication":
                $this->doMultiplication();
                break;

            case "Division":
                $this->doDivision();
                break;
        }
    }

    public function getResult()
    {
        $this -> setResult();
        return $this->result;
    }

    public function doAddition() {
        return $this->result = $this->number1 + $this->number2;
    }

    public function doSubtraction() {
        return $this->result = $this->number1 - $this->number2;
    }

    public function doMultiplication() {
        return $this->result = $this->number1 * $this->number2;
    }

    public function doDivision() {
        return $this->result = $this->number1 / $this->number2;
    }

}