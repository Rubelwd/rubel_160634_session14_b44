<?php

require_once("../../vendor/autoload.php");

$objCalculator = new \App\Calculator();
$objCalculator->setNumber1($_POST['number1']);
$objCalculator->setNumber2($_POST['number2']);
$objCalculator->setOperation($_POST['operation']);

echo $objCalculator->getResult();